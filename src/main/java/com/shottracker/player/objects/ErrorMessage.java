package com.shottracker.player.objects;

public class ErrorMessage {
    public String error;
    public String error_description;
    public int status;
    public String message;

    public ErrorMessage(String error, String error_description) {
        this.error = error;
        this.error_description = error_description;
    }
}
