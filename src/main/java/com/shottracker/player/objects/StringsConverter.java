package com.shottracker.player.objects;


import com.google.gson.Gson;

import org.greenrobot.greendao.converter.PropertyConverter;

import java.util.ArrayList;

public class StringsConverter implements PropertyConverter<ArrayList<String>, String> {
    @Override
    public ArrayList<String> convertToEntityProperty(String databaseValue) {
        //noinspection InstantiatingObjectToGetClassObject
        return new Gson().fromJson(databaseValue, new ArrayList<String>().getClass());
    }

    @Override
    public String convertToDatabaseValue(ArrayList entityProperty) {
        return new Gson().toJson(entityProperty);
    }
}
