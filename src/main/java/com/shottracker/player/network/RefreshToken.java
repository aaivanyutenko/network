package com.shottracker.player.network;

import com.android.volley.Response;
import com.google.gson.JsonObject;

class RefreshToken extends Authorization {
    RefreshToken(Service service, Response.Listener<JsonObject> listener, ErrorListener errorListener) {
        super(service, "oauth/token", listener, errorListener);
        parameters.put("grant_type", "refresh_token");
        parameters.put("refresh_token", service.preferences().getRefreshToken());
    }
}
