package com.shottracker.player.network.profile;

import android.annotation.SuppressLint;

import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.network.HttpRequest;
import com.shottracker.player.network.Service;

public class UserProfile extends HttpRequest {
    @SuppressLint("DefaultLocale")
    public UserProfile(Service service, int method, String userName, JsonObject payload, Response.Listener<JsonObject> listener, ErrorListener errorListener) {
        super(service, method, HttpRequest.ROOT, String.format("playerProfile/%s/", userName), listener, errorListener);
        body = payload;
    }
}
