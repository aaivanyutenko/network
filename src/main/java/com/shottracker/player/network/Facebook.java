package com.shottracker.player.network;

import com.android.volley.Response;
import com.google.gson.JsonObject;

public class Facebook extends Authorization {
    public Facebook(Service service, String token, Response.Listener<JsonObject> listener, ErrorListener errorListener) {
        super(service, "oauth/facebook/player/", listener, errorListener);
        getHeaders().put("facebook-token", token);
    }
}
