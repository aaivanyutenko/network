package com.shottracker.player.network.homecourt;

import android.annotation.SuppressLint;

import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.network.Service;
import com.shottracker.player.network.HttpRequest;

public class HomeCourtActivity extends HttpRequest {
    @SuppressLint("DefaultLocale")
    public HomeCourtActivity(Service service, Response.Listener<JsonObject> listener, ErrorListener errorListener) {
        super(service, Method.GET, HttpRequest.ROOT, String.format("homecourtService/player/status/%d/", 20), listener, errorListener);
    }
}
