package com.shottracker.player.network.homecourt;

import android.annotation.SuppressLint;

import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.network.Service;
import com.shottracker.player.network.HttpRequest;

public class HomeCourtActivityNew extends HttpRequest {
    @SuppressLint("DefaultLocale")
    public HomeCourtActivityNew(Service service, long timestamp, Response.Listener<JsonObject> listener, ErrorListener errorListener) {
        super(service, Method.GET, HttpRequest.ROOT, String.format("homecourtService/player/status/new/topItem/%d/", timestamp), listener, errorListener);
    }
}
