package com.shottracker.player.network.homecourt;

import android.annotation.SuppressLint;

import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.network.Service;
import com.shottracker.player.network.HttpRequest;

public class HomeCourtActivityNext extends HttpRequest {
    @SuppressLint("DefaultLocale")
    public HomeCourtActivityNext(Service service, int offset, long timestamp, Response.Listener<JsonObject> listener, ErrorListener errorListener) {
        super(service, Method.GET, HttpRequest.ROOT, String.format("homecourtService/player/status/next/%d/%d/topItem/%d/", 20, offset, timestamp), listener, errorListener);
    }
}
