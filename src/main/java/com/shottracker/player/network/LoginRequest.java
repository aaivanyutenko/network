package com.shottracker.player.network;

import com.android.volley.Response;
import com.google.gson.JsonObject;

public class LoginRequest extends Authorization {
    public LoginRequest(Service service, String username, String password, Response.Listener<JsonObject> listener, ErrorListener errorListener) {
        super(service, "oauth/token", listener, errorListener);
        parameters.put("grant_type", "password");
        parameters.put("username", username);
        parameters.put("password", password);
    }
}
