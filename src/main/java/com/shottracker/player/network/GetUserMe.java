package com.shottracker.player.network;

import com.android.volley.Response;
import com.google.gson.JsonObject;

public class GetUserMe extends HttpRequest {
    public GetUserMe(Service service, Response.Listener<JsonObject> listener, ErrorListener errorListener) {
        super(service, Method.GET, HttpRequest.ROOT, "userService/user/me/", listener, errorListener);
    }
}
