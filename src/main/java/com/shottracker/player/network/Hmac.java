package com.shottracker.player.network;

import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.objects.ErrorMessage;

public class Hmac extends HttpRequest {
    private static final String TAG = Hmac.class.getSimpleName();

    public Hmac(final Service service, int method, final String email, @NonNull final ErrorListener errorListener) {
        super(service, method, HttpRequest.TEAM_ROOT, "fan", new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
                Log.d(TAG, "onResponse() called with: response = [" + response + "]");
            }
        }, method == Method.POST ? errorListener : new ErrorListener() {
            @Override
            public void onErrorResponse(ErrorMessage errorMessage) {
                if (errorMessage.status == 404) {
                    service.queue().add(new Hmac(service, Request.Method.POST, email, errorListener));
                }
            }
        });
        body = new JsonObject();
        body.addProperty("email", email);
    }
}
