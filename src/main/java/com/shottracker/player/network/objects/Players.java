package com.shottracker.player.network.objects;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.network.HttpRequest;
import com.shottracker.player.network.Service;

public class Players extends HttpRequest {
    @SuppressLint("DefaultLocale")
    public Players(Service service, long teamId, Response.Listener<JsonObject> listener, @NonNull ErrorListener errorListener) {
        super(service, Method.GET, HttpRequest.ROOT, String.format("team/public/%d/detail/players/", teamId), listener, errorListener);
    }
}
