package com.shottracker.player.network;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.shottracker.player.Preferences;
import com.shottracker.player.objects.ErrorMessage;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpRequest extends Request<JsonObject> {
    public static final String SERVER = String.format("http://%s/", Preferences.HOST);
    public static final String ROOT = "v2/";
    protected static final String TEAM_ROOT = "team/v1/api/";
    private static final JsonParser parser = new JsonParser();
    private static final String TAG = HttpRequest.class.getSimpleName();
    private final Response.Listener<JsonObject> listener;
    protected Map<String, String> parameters = new HashMap<>();
    protected Map<String, String> headers = new HashMap<>();
    protected Service service;
    protected JsonObject body;
    private String root;
    private String url;
    private ErrorListener errorListener;

    public HttpRequest(HttpRequest original) {
        this(original.service, original.getMethod(), original.root, original.url, original.listener, original.errorListener);
        body = original.body;
    }

    public HttpRequest(final Service service, final int method, final String root, final String url, final Response.Listener<JsonObject> listener, @NonNull final ErrorListener errorListener) {
        super(method, SERVER + root + url, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse != null) {
                    String data = new String(error.networkResponse.data);
                    ErrorMessage errorMessage;
                    try {
                        errorMessage = new Gson().fromJson(data, ErrorMessage.class);
                    } catch (JsonSyntaxException e) {
                        errorMessage = new ErrorMessage(error.toString(), data);
                    }
                    String wwwAuthHeader = error.networkResponse.headers.get("WWW-Authenticate");
                    if (service.preferences().getRefreshToken() != null && error.networkResponse.statusCode == 401 && (errorMessage != null && "invalid_token".equals(errorMessage.error.toLowerCase()) || wwwAuthHeader != null && (wwwAuthHeader.toLowerCase().contains("invalid_token") || !wwwAuthHeader.contains("MACC realm")))) {
                        final List<HttpRequest> cancelled = new ArrayList<>();
                        service.queue().cancelAll(new RequestQueue.RequestFilter() {
                            @Override
                            public boolean apply(Request<?> request) {
                                if (request instanceof HttpRequest) {
                                    cancelled.add(new HttpRequest((HttpRequest) request));
                                    return true;
                                }
                                return false;
                            }
                        });
                        service.queue().cancelAll(HttpRequest.class);
                        service.queue().add(new RefreshToken(service, new Response.Listener<JsonObject>() {
                            @Override
                            public void onResponse(JsonObject response) {
                                for (HttpRequest request : cancelled) {
                                    service.queue().add(request.refreshToken());
                                }
                            }
                        }, new ErrorListener() {
                            @Override
                            public void onErrorResponse(ErrorMessage errorMessage) {
                                errorListener.onErrorResponse(errorMessage);
                            }
                        }));
                    } else if (error.networkResponse.statusCode == 400 && (errorMessage != null && "invalid_grant".equals(errorMessage.error.toLowerCase()))) {
                        service.logout();
                    } else {
                        if (errorMessage == null) {
                            String type = "http error " + error.networkResponse.statusCode;
                            String details = error.networkResponse.headers.get("X-Android-Response-Source");
                            errorMessage = new ErrorMessage(type, details == null ? type : details);
                        }
                        errorListener.onErrorResponse(errorMessage);
                    }
                } else {
                    ErrorMessage errorMessage;
                    if (error.getCause() != null) {
                        errorMessage = new ErrorMessage(error.getCause().getClass().getName(), error.getMessage());
                    } else {
                        if (error instanceof TimeoutError) {
                            errorMessage = new ErrorMessage(error.toString(), "Network error: timeout expired");
                        } else {
                            errorMessage = new ErrorMessage(error.toString(), error.toString());
                        }
                    }
                    errorListener.onErrorResponse(errorMessage);
                }
            }
        });
        setTag(HttpRequest.class);
        this.errorListener = errorListener;
        this.listener = listener;
        this.service = service;
        this.root = root;
        this.url = url;
        headers.put("Accept", "application/json");
        headers.put("api-version", "2.2");
        headers.putAll(service.getAppHeaders());
        refreshToken();
    }

    private HttpRequest refreshToken() {
        String token = service.preferences().getAccessToken();
        if (!TextUtils.isEmpty(token)) {
            String type = service.preferences().getTokenType();
            headers.put("Authorization", type + " " + token);
        } else if (service.preferences().isHmac()) {
            headers.put("Authorization", service.preferences().getHmac());
        }
        return this;
    }

    @Override
    protected void deliverResponse(JsonObject response) {
        if (ROOT.equals(root) && response.has("code") && response.has("status") && response.get("data").isJsonObject()) {
            if (response.get("code").getAsInt() == 200 && "success".equals(response.get("status").getAsString())) {
                listener.onResponse(response.getAsJsonObject("data"));
            } else {
                ErrorMessage message = new ErrorMessage(response.get("status").getAsString(), response.get("message").getAsString());
                errorListener.onErrorResponse(message);
            }
        } else {
            listener.onResponse(response);
        }
    }

    @Override
    protected Response<JsonObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            return Response.success(parser.parse(json).getAsJsonObject(), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    @Override
    public Map<String, String> getParams() {
        return parameters;
    }

    @Override
    public Map<String, String> getHeaders() {
        return headers;
    }

    @Override
    public String getBodyContentType() {
        if (body == null) {
            return super.getBodyContentType();
        } else {
            return "application/json; charset=utf-8";
        }
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        if (body == null) {
            return super.getBody();
        } else {
            try {
                return body.toString().getBytes("utf-8");
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG, "getBody: UnsupportedEncodingException", e);
                return null;
            }
        }
    }

    public interface ErrorListener {
        void onErrorResponse(ErrorMessage errorMessage);
    }
}
