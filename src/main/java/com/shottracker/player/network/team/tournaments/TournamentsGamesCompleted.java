package com.shottracker.player.network.team.tournaments;

import android.annotation.SuppressLint;

import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.network.HttpRequest;
import com.shottracker.player.network.Service;

public class TournamentsGamesCompleted extends HttpRequest {
    @SuppressLint("DefaultLocale")
    public TournamentsGamesCompleted(Service service, String tournamentId, Response.Listener<JsonObject> listener, ErrorListener errorListener) {
        super(service, Method.GET, HttpRequest.TEAM_ROOT, String.format("tournaments/%s/games/completed", tournamentId), listener, errorListener);
    }
}
