package com.shottracker.player.network.team.stats;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.network.HttpRequest;
import com.shottracker.player.network.Service;

public class PracticesHistoryScore extends HttpRequest {
    @SuppressLint("DefaultLocale")
    public PracticesHistoryScore(Service service, long teamId, long playerId, Response.Listener<JsonObject> listener, @NonNull ErrorListener errorListener) {
        super(service, Method.GET, HttpRequest.TEAM_ROOT, String.format("stats/practices/history/score?teamId=%d&playerId=%d", teamId, playerId), listener, errorListener);
    }
}
