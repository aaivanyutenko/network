package com.shottracker.player.network.team.stats;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.network.HttpRequest;
import com.shottracker.player.network.Service;

public class GamesRangeShots extends HttpRequest {
    @SuppressLint("DefaultLocale")
    public GamesRangeShots(Service service, long teamId, long from, long to, Response.Listener<JsonObject> listener, @NonNull ErrorListener errorListener) {
        super(service, Method.GET, HttpRequest.TEAM_ROOT, String.format("stats/games/range/shots?teamId=%d&from=%d&to=%d", teamId, from, to), listener, errorListener);
    }
}
