package com.shottracker.player.network.team.facilities;

import android.annotation.SuppressLint;

import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.network.HttpRequest;
import com.shottracker.player.network.Service;

public class CourtsSessions extends HttpRequest {
    @SuppressLint("DefaultLocale")
    public CourtsSessions(Service service, String facilityId, String courtId, String sessionId, Response.Listener<JsonObject> listener, ErrorListener errorListener) {
        super(service, Method.GET, HttpRequest.TEAM_ROOT, String.format("facilities/%s/courts/%s/sessions/%s", facilityId, courtId, sessionId), listener, errorListener);
    }
}
