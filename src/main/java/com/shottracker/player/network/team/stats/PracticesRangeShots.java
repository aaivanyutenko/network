package com.shottracker.player.network.team.stats;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.network.HttpRequest;
import com.shottracker.player.network.Service;

public class PracticesRangeShots extends HttpRequest {
    @SuppressLint("DefaultLocale")
    public PracticesRangeShots(Service service, long teamId, long from, long to, String sessionType, Response.Listener<JsonObject> listener, @NonNull ErrorListener errorListener) {
        super(service, Method.GET, HttpRequest.TEAM_ROOT, String.format("stats/practices/range/shots/?teamId=%d&from=%d&to=%d%s", teamId, from, to, sessionType == null ? "" : ("&sessionType=" + sessionType)), listener, errorListener);
    }
}
