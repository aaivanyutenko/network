package com.shottracker.player.network.team.stats;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.network.HttpRequest;
import com.shottracker.player.network.Service;

public class GamesScore extends HttpRequest {
    @SuppressLint("DefaultLocale")
    public GamesScore(Service service, long teamId, String gameId, Response.Listener<JsonObject> listener, @NonNull ErrorListener errorListener) {
        super(service, Method.GET, HttpRequest.TEAM_ROOT, String.format("stats/games/%s/score/?teamId=%d", gameId, teamId), listener, errorListener);
    }
}
