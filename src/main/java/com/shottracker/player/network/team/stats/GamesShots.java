package com.shottracker.player.network.team.stats;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.network.HttpRequest;
import com.shottracker.player.network.Service;

public class GamesShots extends HttpRequest {
    @SuppressLint("DefaultLocale")
    public GamesShots(Service service, String gameId, long teamId, Response.Listener<JsonObject> listener, @NonNull ErrorListener errorListener) {
        super(service, Method.GET, HttpRequest.TEAM_ROOT, String.format("stats/games/%s/shots/?teamId=%d", gameId, teamId), listener, errorListener);
    }
}
