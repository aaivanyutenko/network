package com.shottracker.player.network.team.facilities;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.network.HttpRequest;
import com.shottracker.player.network.Service;

public class Facilities extends HttpRequest {
    @SuppressLint("DefaultLocale")
    public Facilities(Service service, long teamId, Response.Listener<JsonObject> listener, @NonNull ErrorListener errorListener) {
        super(service, Method.GET, HttpRequest.TEAM_ROOT, String.format("facilities?teamId=%d", teamId), listener, errorListener);
    }
}
