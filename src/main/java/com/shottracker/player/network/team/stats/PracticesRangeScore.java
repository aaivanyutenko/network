package com.shottracker.player.network.team.stats;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.network.HttpRequest;
import com.shottracker.player.network.Service;

public class PracticesRangeScore extends HttpRequest {
    @SuppressLint("DefaultLocale")
    public PracticesRangeScore(Service service, long teamId, long playerId, long from, long to, String sessionType, Response.Listener<JsonObject> listener, @NonNull ErrorListener errorListener) {
        super(service, Method.GET, HttpRequest.TEAM_ROOT, String.format("stats/practices/range/score/?teamId=%d%s&from=%d&to=%d%s", teamId, playerId > 0 ? ("&playerId=" + playerId) : "", from, to, sessionType == null ? "" : ("&sessionType=" + sessionType)), listener, errorListener);
    }
}
