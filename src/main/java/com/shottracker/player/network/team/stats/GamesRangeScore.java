package com.shottracker.player.network.team.stats;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.network.HttpRequest;
import com.shottracker.player.network.Service;

public class GamesRangeScore extends HttpRequest {
    @SuppressLint("DefaultLocale")
    public GamesRangeScore(Service service, long teamId, long playerId, long from, long to, Response.Listener<JsonObject> listener, @NonNull ErrorListener errorListener) {
        super(service, Method.GET, HttpRequest.TEAM_ROOT, String.format("stats/games/range/score?teamId=%d&playerId=%d&from=%d&to=%d", teamId, playerId, from, to), listener, errorListener);
    }
}
