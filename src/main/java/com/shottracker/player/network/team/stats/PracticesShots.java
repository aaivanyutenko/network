package com.shottracker.player.network.team.stats;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.network.HttpRequest;
import com.shottracker.player.network.Service;

public class PracticesShots extends HttpRequest {
    @SuppressLint("DefaultLocale")
    public PracticesShots(Service service, String practiceId, long teamId, String sessionType, Response.Listener<JsonObject> listener, @NonNull ErrorListener errorListener) {
        super(service, Method.GET, HttpRequest.TEAM_ROOT, String.format("stats/practices/%s/shots/?teamId=%d&sessionType=%s", practiceId, teamId, sessionType), listener, errorListener);
    }
}
