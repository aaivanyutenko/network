package com.shottracker.player.network.team.facilities;

import android.support.annotation.NonNull;

import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.network.HttpRequest;
import com.shottracker.player.network.Service;

public class Courts extends HttpRequest {
    public Courts(Service service, String facilityId, Response.Listener<JsonObject> listener, @NonNull ErrorListener errorListener) {
        super(service, Method.GET, HttpRequest.TEAM_ROOT, String.format("facilities/%s/courts", facilityId), listener, errorListener);
    }
}
