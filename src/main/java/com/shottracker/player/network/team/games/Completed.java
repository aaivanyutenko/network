package com.shottracker.player.network.team.games;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.network.HttpRequest;
import com.shottracker.player.network.Service;

public class Completed extends HttpRequest {
    @SuppressLint("DefaultLocale")
    public Completed(Service service, String tournamentId, long teamId, Response.Listener<JsonObject> listener, @NonNull ErrorListener errorListener) {
        super(service, Method.GET, HttpRequest.TEAM_ROOT, String.format("games/completed/?teamId=%d%s", teamId, tournamentId == null ? "" : ("&tournamentId=" + tournamentId)), listener, errorListener);
    }
}
