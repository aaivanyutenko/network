package com.shottracker.player.network;

import com.android.volley.RequestQueue;
import com.shottracker.player.Preferences;

import java.util.Map;

public interface Service {
    RequestQueue queue();
    Preferences preferences();
    void logout();
    Map<String, String> getAppHeaders();
}
