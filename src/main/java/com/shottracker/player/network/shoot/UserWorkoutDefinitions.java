package com.shottracker.player.network.shoot;

import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.network.Service;
import com.shottracker.player.network.HttpRequest;

public class UserWorkoutDefinitions extends HttpRequest {
    public UserWorkoutDefinitions(Service service, String userName, Response.Listener<JsonObject> listener, ErrorListener errorListener) {
        super(service, Method.GET, HttpRequest.ROOT, String.format("workout/options/detail/%s/", userName), listener, errorListener);
    }
}
