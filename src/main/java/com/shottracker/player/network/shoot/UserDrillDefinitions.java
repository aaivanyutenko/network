package com.shottracker.player.network.shoot;

import com.android.volley.Response;
import com.google.gson.JsonObject;
import com.shottracker.player.network.Service;
import com.shottracker.player.network.HttpRequest;

public class UserDrillDefinitions extends HttpRequest {
    public UserDrillDefinitions(Service service, String userName, Response.Listener<JsonObject> listener, ErrorListener errorListener) {
        super(service, Method.GET, HttpRequest.ROOT, String.format("drill/options/detail/%s/", userName), listener, errorListener);
    }
}
