package com.shottracker.player.network;

import com.android.volley.Response;
import com.google.gson.JsonObject;

class Authorization extends HttpRequest {
    Authorization(Service service, String url, Response.Listener<JsonObject> listener, ErrorListener errorListener) {
        super(service, Method.POST, HttpRequest.ROOT, url, listener, errorListener);
        parameters.put("client_id", "shottrackerteamapp");
    }

    @Override
    protected void deliverResponse(JsonObject response) {
        String accessToken = response.get("access_token").getAsString();
        String refreshToken = response.get("refresh_token").getAsString();
        String tokenType = response.get("token_type").getAsString();
        service.preferences().setTokens(accessToken, refreshToken, tokenType);
        super.deliverResponse(response);
    }
}
