package com.shottracker.player;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.util.Base64;

import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class Preferences {
    private static final boolean PROD = false;
    public static final String HOST = PROD ? "app.shottracker.com" : "devapp.shottracker.com";
    public static final String HMAC_SECRET = PROD ? "neTSS2NmBnxbA9LRPrhkNnExdW2D4Gjp" : "tujzTV6CjqboYMHTET8TVwQLD8bDe44Q";
    public static final String LIVE_SOCKET_URL = String.format("ws://%s/live", HOST);
    private SharedPreferences preferences;

    public Preferences(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    public String getAccessToken() {
        return preferences.getString("access_token", null);
    }

    public String getRefreshToken() {
        return preferences.getString("refresh_token", null);
    }

    public String getTokenType() {
        return preferences.getString("token_type", null);
    }

    public Map<String, String> getSocketHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "STFanApp;v1.0");
        if (isHmac()) {
            headers.put("Authorization", getHmac());
        } else {
            headers.put("Authorization", getTokenType() + " " + getAccessToken());
        }
        return headers;
    }

    @SuppressLint("DefaultLocale")
    public String getHmac() {
        long ts = System.currentTimeMillis();
        UUID nonce = UUID.randomUUID();
        String key = "fan-app-android";
        String data = ts + nonce.toString();
        String secret = HMAC_SECRET;
        String token;
        String mac = null;
        try {
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            token = Base64.encodeToString(sha256_HMAC.doFinal(data.getBytes("UTF-8")), Base64.NO_WRAP);
            mac = String.format("MAC id=%s,ts=%d,nonce=%s,mac=%s", key, ts, nonce.toString(), token);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException | InvalidKeyException ignored) {
        }
        return mac;
    }

    public void setTokens(String accessToken, String refreshToken, String tokenType) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("access_token", accessToken);
        editor.putString("refresh_token", refreshToken);
        editor.putString("token_type", tokenType);
        editor.apply();
    }

    public void setFilter(Map<String, Boolean> filter) {
        preferences.edit().putString("filter_preference", new Gson().toJson(filter)).apply();
    }

    public void refreshFilter() {
        preferences.edit().putString("filter_preference", null).apply();
    }

    public HashMap<String, Boolean> getFilter() {
        //noinspection InstantiatingObjectToGetClassObject
        return new Gson().fromJson(preferences.getString("filter_preference", "{WORKOUTS:true,DRILLS:true,FORWARD:true,CENTER:true,GUARD:true,EASY:true,MEDIUM:true,HARD:true}"), new HashMap<String, Boolean>().getClass());
    }

    public boolean isFirst() {
        return preferences.getBoolean("is_first", true);
    }

    public void setFirst(boolean first) {
        preferences.edit().putBoolean("is_first", first).apply();
    }

    public void setHmac(boolean hmac) {
        preferences.edit().putBoolean("is_hmac", hmac).apply();
    }

    public boolean isHmac() {
        return preferences.getBoolean("is_hmac", false);
    }
}
